﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {


	public GameObject[] blockTiles;
	public List<GameObject> boardBlocks = new List<GameObject>();

	int countElements = 0;

	int countBlockTypes = 4;

	public int columns = 5;
	public int rows = 5;

	public List<Vector3> gridPositions = new List<Vector3>();
	public int[,] boardLevel;
	private Transform boardHolder;

	void InitialiseList()
	{
		gridPositions.Clear ();
		for (int x = 0; x < columns; x++) {
			for (int y = 0; y < rows; y++) {
				gridPositions.Add(new Vector3(x,y,0f));
			}
		}
	}

	void boardSetup()
	{
		boardHolder = new GameObject ("Board").transform;
		boardLevel = new int[columns, rows];
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				boardLevel[i,j] = 0;
			}
		}
	}

	bool generateRangomElement()
	{
		if (countElements >= columns * rows) 
		{
			return false;
		}

		int randomX = Random.Range (0, columns - 1);
		int randomY = Random.Range (0, rows - 1);
		while (boardLevel[randomX, randomY] == 0) 
		{
			randomX = Random.Range (0, columns - 1);
			randomY = Random.Range (0, rows - 1);
		}
		boardLevel [randomX, randomY] = Random.Range (0, countBlockTypes - 1);
		boardBlocks.Add(


	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
